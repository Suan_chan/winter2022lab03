public class Boba{
 public String base;
 public String flavour;
 public String toppings;
 public int sweet;
 
 public void drink(String base, String flavour, String topping){
  if(this.base.equals("milk")) {
   if(this.flavour.equals("tea")){
    if(this.toppings.equals("tapioca")){
     System.out.println("I would reccommend Thai Milk Tea with tapioca");
    } else if(this.toppings.equals("grass jelly")){
     System.out.println("I would reccommend Milk Tea with grass jelly");
    } else if(this.toppings.equals("none")){
     System.out.println("I would reccomend Milk Tea");
    } else {
     System.out.println("Sorry, your topping isn't availble for your drink");
    }
   } else if(this.flavour.equals("strawberry") || this.flavour.equals("honeydew") || this.flavour.equals("mango")){
    if(this.toppings.equals("tapioca")){
     System.out.println("I would reccomend" + this.flavour + " Milk Tea with tapioca");
    } else if(this.toppings.equals("none")){
     System.out.println("I would reccomend" + this.flavour + " Milk Tea");
    } else {
     System.out.println("Sorry, your topping isn't availble for your drink");
    }
   } else {
    System.out.println("Sorry. This flavour is unavailbale.");
   }
  } else if(this.base.equals("juice")) {
   if(this.flavour.equals("strawberry") || this.flavour.equals("honeydew") || this.flavour.equals("mango")){
    if(this.toppings.equals("tapioca")){
     System.out.println("I would reccommend " + this.flavour + " boba with tapioca");
    } else if(this.toppings.equals("lychee jelly")){
     System.out.println("I would reccommend " + this.flavour + " boba with lychee jelly");
    } else if(this.toppings.equals("none")){
     System.out.println("I would reccomend " + this.flavour + "boba");
    } else {
     System.out.println("Sorry, your topping isn't availble for your drink");
    }
   } else {
    System.out.println("Sorry. This flavour is unavailbale.");
   }
  } else{
   System.out.println("Please retype your base(milk/tea)");
  }
 }
}