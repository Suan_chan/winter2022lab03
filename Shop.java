import java.util.Scanner;

public class Shop{
  public static void main(String[] args){
    Scanner reader = new Scanner(System.in);
    Boba sc = new Boba();
    Boba[] customer = new Boba[4];
    
    for (int i = 0; i < customer.length; i++){
      //Welcome Message + Menu
      System.out.println("Welcome to the boba shop! Here's some specifications: ");
      System.out.println("Toppings: tapioca, grass jelly (only applicable for milk tea), lychee jelly (only applicable for fruit juice) and none");
      System.out.println("Flavour: tea (only applicable for milk base) and fruit");
      
      //User inputs
      customer[i] = new Boba();
      System.out.println("Would you like tea or milk base? (ex: milk/juice)");
      customer[i].base = reader.nextLine();
      System.out.println("What fruit flavour would you like (ex: strawberry/honeydew/mango)? If not, just write: tea (tea is only applicable to a milk base)");
      customer[i].flavour = reader.nextLine();
      System.out.println("Which topping would you like? (ex: tapioca/grass jelly/lychee jelly/none)");
      customer[i].toppings = reader.nextLine();
      System.out.println("What percentage or sugar would you like? (ex: 30, 50, 75, 100, 125)");
      customer[i].sweet = Integer.parseInt(reader.nextLine());
    }
    
    String lastBase = customer[customer.length - 1].base;
    String lastFlavour = customer[customer.length - 1].flavour;
    String lastTopping = customer[customer.length - 1].toppings;
    int lastSweet = customer[customer.length - 1].sweet;
    
    System.out.println("Base: " + lastBase);
    System.out.println("Flavour: " + lastFlavour);
    System.out.println("Toppings: " + lastTopping);
    System.out.println("Sweet %: " + lastSweet);
    System.out.println("---");
    customer[customer.length - 1].drink(lastBase, lastFlavour, lastTopping);
  }
}